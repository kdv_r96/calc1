using System;

namespace SimpleCalculator
{
 
        class Program
        {
            static void Main()
            {
            Console.Title = "Калькулятор";
            Calc c = new Calc();
                int ans = c.Add(10, 84);
                Console.WriteLine("10 + 84 is {0}.", ans);
                // Wait for user to press the Enter key before shutting down.
                Console.ReadLine();
                 Console.ReadKey();

                 // добвил новый комментарий
        }
        }

        // The C# calculator.
        class Calc
        {
            public int Add(int x, int y)
            { return x + y; }
        }
    }
